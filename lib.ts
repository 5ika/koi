import { Task } from "./types.d.ts";

export const parseLine = (rawTask: string, index: number): Task => {
  const [rawDone, title, rawDate, rawTags] = rawTask.split(";");
  const done = rawDone === "x";
  const tags = rawTags ? rawTags?.split(/, ?/) : [];
  const date = rawDate ? rawDate : undefined;
  return { index, done, title, date, tags };
};

export const taskToString = (task: InputTask) => {
  const rawTags = task.tags?.join(",");
  const rawDone = task.done ? "x" : "";
  return `${rawDone};${task.title};${task.date || ""};${rawTags || ""}`;
};

export const checkDate = (rawDate: string) => {
  const dateRegex = /(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(\d{4})/;
  if (!dateRegex.test(rawDate))
    throw new Error("Date must respect format DD-MM-YYYY");
  else return rawDate;
};

export const parseDate = (rawDate: string) => {
  const dateRegex = /(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(\d{4})/;
  const [_, day, month, year] = rawDate.match(dateRegex) || [];
  return new Date(`${year}-${month}-${day}`);
};

export const HOME_DIRECTORY = Deno.env.get("HOME");
export const KOI_DIRECTORY = `${HOME_DIRECTORY}/.koi`;
export const KOI_FILENAME = "tasks";
export const FILE_PATH = `${KOI_DIRECTORY}/${KOI_FILENAME}`;

type Task = {
  index: number;
  done: boolean;
  title: string;
  date?: string;
  tags?: string[];
};

type InputTask = {
  done?: boolean;
  title: string;
  date?: string;
  tags?: string[];
};

type Action = (cmds: ActionCmds, params: ActionParams) => Promise<string>;

type ActionCmds = string[];

type ActionParams = Record<string, string | number>;

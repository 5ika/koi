Kōi helps you manage your tasks list across multiple devices.

# Install

```shell
deno install --allow-read --allow-write --allow-env=HOME --allow-run https://gitlab.com/5ika/koi/-/raw/main/mod.ts
```

# Configure
Using an empty remote git repository:

```shell
koi git init <remote git URL>
```

Using an existing remote git repository:

```shell
koi git clone <remote git URL>
```

# Use
## List tasks
```shell
koi list                  List all tasks
koi ls                    Alias
koi list --tag Work       List tasks with tag 'Work'
```

## Add task
```shell
koi add "My first task"
koi add "My new task" 29-07-2022 Work Project1
koi add "Another task" '' Perso
```

## Done / Undone task
```shell
# Done
koi done 2
koi d 2

# Undone
koi undone 3
koi u 3
```

## Update task
```shell
koi update 2 "New title"
koi update 1 "New title" 10-08-2022
koi update 16 "Do something" '' NewTag
koi set 4 "New title"
```

## Remove task
```shell
koi remove 2
koi rm 5
```

## Save tasks to remote git
```shell
koi git push
```

## Retrieve tasks from remote git
```shell
koi git pull
```

## Show help
```shell
koi help
koi h
```

# Roadmap

- [x] Add tasks file synchronization with git
- [x] Allow to set task as "Done" and store history
- [ ] Allow to sort tasks by date
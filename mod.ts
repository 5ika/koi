#!/usr/bin/env -S deno run --allow-read --allow-write --allow-env=HOME --allow-run

import { parse, terminal } from "./deps.ts";
import actions from "./actions/index.ts";

const { _: commands, ...params } = parse(Deno.args);
const [mainCommand, ...cmds] = commands;

const availableActions = Object.keys(actions);

if (availableActions.includes(mainCommand)) {
  const result = await actions[mainCommand](cmds, params);
  terminal.log(result);
} else {
  const result = await actions.help(cmds, params);
  terminal.log(result);
}

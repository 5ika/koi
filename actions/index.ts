import { list } from "./list.ts";
import { add } from "./add.ts";
import { remove } from "./remove.ts";
import { update } from "./update.ts";
import { help } from "./help.ts";
import { git } from "./git.ts";
import { done, undone } from "./done.ts";

interface Actions {
  [key: string]: Action;
}

const actions: Actions = {
  list,
  add,
  remove,
  update,
  done,
  undone,
  help,
  git,

  // Aliases
  ls: list,
  l: list,
  rm: remove,
  set: update,
  d: done,
  u: undone,
  h: help,
};

export default actions;

import { getDatabaseContent, putDatabaseContent } from "../storage.ts";
import { parseLine, taskToString } from "../lib.ts";

export const done: Action = async (cmds, _params) => {
  const [rawDoneIndex] = cmds;
  await updateTask(rawDoneIndex, true);
  return `Task <inv>[${rawDoneIndex}]</inv> set as done`;
};

export const undone: Action = async (cmds, _params) => {
  const [rawDoneIndex] = cmds;
  await updateTask(rawDoneIndex, false);
  return `Task <inv>[${rawDoneIndex}]</inv> set as not done`;
};

const updateTask = async (rawDoneIndex: string, done = false) => {
  const doneIndex = parseInt(rawDoneIndex);
  if (isNaN(doneIndex)) throw new Error(`${rawDoneIndex} is not a valid index`);

  const rawTasks = await getDatabaseContent();
  const lines = rawTasks.split("\n");
  const tasks = lines.map((line, index) => {
    if (index !== doneIndex) return line;
    const currentTask = parseLine(line, index);
    return taskToString({
      ...currentTask,
      done,
    });
  });

  await putDatabaseContent(tasks.join("\n"));
};

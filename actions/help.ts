import type { Action } from "../types.d.ts";

export const help: Action = () => {
  return Promise.resolve(helpText);
};

const helpText = `
<red>Kōi</red> helps you to manage your tasks list accross multiple devices.

<yellow>List tasks</yellow>
koi list                  <dim>List all tasks</dim>
koi ls                    <dim>Alias</dim>
koi list --tag Work       <dim>List tasks with tag 'Work'</dim>

<yellow>Add task</yellow>
koi add "My first task"
koi add "My new task" 29-07-2022 Work Project1
koi add "Another task" '' Perso

<yellow>Update task</yellow>
koi update 2 "New title"
koi update 1 "New title" 10-08-2022
koi update 16 "Do something" '' NewTag
koi set 4 "New title"

<yellow>Remove task</yellow>
koi remove 2
koi rm 5

<yellow>Show help</yellow>
koi help
koi h
`;

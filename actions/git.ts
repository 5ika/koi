import type { Action } from "../types.d.ts";
import * as gitUtils from "../git.ts";

export const git: Action = async (cmds: ActionCmds, _params: ActionParams) => {
  const [subAction, ...actionCmds] = cmds;

  switch (subAction) {
    case "init": {
      const [repositoryUrl] = actionCmds;
      return await gitUtils.init(repositoryUrl);
    }
    case "clone": {
      const [repositoryUrl] = actionCmds;
      return await gitUtils.clone(repositoryUrl);
    }
    case "push": {
      const [branch] = actionCmds;
      return await gitUtils.push(branch);
    }
    case "pull":
      return await gitUtils.pull();
    default:
      return Promise.resolve(`Error: git command not found (${subAction})`);
  }
};

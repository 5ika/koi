import { taskToString, checkDate } from "../lib.ts";
import { addItemToDatabase } from "../storage.ts";

export const add: Action = async (cmds, _params) => {
  const [title, date, ...tags] = cmds;
  if (!title) throw new Error("Please provide a title");
  if (date) checkDate(date);
  const task: InputTask = {
    title,
    date,
    tags,
  };
  const taskString = taskToString(task);
  await addItemToDatabase(taskString);
  return `Task <b>'${title}'</b> added`;
};

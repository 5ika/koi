import type { Action } from "../types.d.ts";
import { getDatabaseContent, putDatabaseContent } from "../storage.ts";

export const remove: Action = async (cmds, _params) => {
  const [rawRemovedIndex] = cmds;

  const removedIndex = parseInt(rawRemovedIndex);
  if (isNaN(removedIndex))
    throw new Error(`${rawRemovedIndex} is not a valid index`);

  const rawTasks = await getDatabaseContent();
  const lines = rawTasks.split("\n");
  const tasks = lines.filter((_line, index) => index !== removedIndex);
  await putDatabaseContent(tasks.join("\n"));

  return `Task <inv>[${removedIndex}]</inv> has been removed`;
};

import { parseLine, parseDate } from "../lib.ts";
import { getDatabaseContent } from "../storage.ts";

type ListParams = {
  tag?: string;
};

export const list: Action = async (_cmds, params: ListParams) => {
  const rawTasks = await getDatabaseContent();
  const lines = rawTasks.split("\n");
  const tasks = lines.filter(line => !!line).map(parseLine);

  if (params.tag) {
    const filteredTasks = tasks.filter(task => task.tags?.includes(params.tag));
    return showTasksList(filteredTasks);
  } else return showTasksList(tasks);
};

const showTasksList = (tasks: Task[]) => {
  let textContent = "\nYour tasks:\n\n";
  tasks.forEach(task => {
    const tagsString = task.tags
      ?.map((t: string) => `<cyan>#${t}</cyan>`)
      .join(" ");
    let taskString = `${task.title} ${showDate(task.date)}${tagsString}\n`;
    if (task.done) taskString = `<s>${taskString}</s>`;
    textContent += `<inv>[${task.index}]</inv> ${taskString}`;
  });
  textContent += "\n";
  return textContent;
};

const showDate = (rawDate?: string) => {
  if (!rawDate) return "";
  const date = parseDate(rawDate);
  const now = new Date();
  if (date > now) return `<green>${rawDate}</green> `;
  else return `<red>${rawDate}</red> `;
};

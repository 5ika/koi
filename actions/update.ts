import { parseLine, taskToString, checkDate } from "../lib.ts";
import { getDatabaseContent, putDatabaseContent } from "../storage.ts";

export const update: Action = async (cmds, _params) => {
  const [rawUpdateIndex, title, date, ...tags] = cmds;

  const updateIndex = parseInt(rawUpdateIndex);
  if (isNaN(updateIndex))
    throw new Error(`${rawUpdateIndex} is not a valid index`);

  const rawTasks = await getDatabaseContent();
  const lines = rawTasks.split("\n");
  const tasks = lines.map((line, index) => {
    if (index !== updateIndex) return line;
    const currentTask = parseLine(line, index);
    if (date) checkDate(date);
    return taskToString({
      title: title || currentTask.title,
      date: date || currentTask.date,
      tags: tags || currentTask.tags,
    });
  });

  await putDatabaseContent(tasks.join("\n"));

  return `Task <inv>[${updateIndex}]</inv> updated`;
};

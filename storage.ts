import { FILE_PATH, KOI_DIRECTORY } from "./config.ts";

export const getDatabaseContent = async () => {
  await validateKoiConfig();
  return await Deno.readTextFile(FILE_PATH);
};

export const putDatabaseContent = async (content: string) => {
  await validateKoiConfig();
  const encoder = new TextEncoder();
  const encodedTask = encoder.encode(content);
  return await Deno.writeFile(FILE_PATH, encodedTask);
};

export const addItemToDatabase = async (item: string) => {
  await validateKoiConfig();
  const encoder = new TextEncoder();
  const encodedTask = encoder.encode(`${item}\n`);
  return await Deno.writeFile(FILE_PATH, encodedTask, { append: true });
};

export const validateKoiConfig = async () => {
  try {
    await Deno.mkdir(KOI_DIRECTORY, { recursive: true });
  } catch (error) {
    console.error("Impossible to create Koi directory: ", error);
  }
  try {
    await Deno.readTextFile(FILE_PATH);
  } catch {
    await Deno.create(FILE_PATH);
    console.log(`Created Koi file at ${FILE_PATH}`);
  }
};

import { terminal } from "./deps.ts";
import { KOI_DIRECTORY, HOME_DIRECTORY } from "./config.ts";
import { validateKoiConfig } from "./storage.ts";

export const init = async (repositoryUrl: string) => {
  if (!repositoryUrl) return `<red>No repository URL provided</red>`;

  try {
    await validateKoiConfig();
    await shellCmd("git init --initial-branch=main");
    await shellCmd(`git remote add origin ${repositoryUrl}`);
    return "<green>Git repository is now initialized</green>";
  } catch (error) {
    console.error(error);
    return `<red>An error occured during git init</red>`;
  }
};

export const clone = async (repositoryUrl: string) => {
  await shellCmd(`git clone ${repositoryUrl} ${KOI_DIRECTORY}`, HOME_DIRECTORY);
  return "<green>Git repository cloned locally</green>";
};

export const push = async (branch = "main") => {
  await shellCmd("git add -A");
  const date = new Date().toISOString();
  await shellCmd(`git commit -m ${date}`);
  await shellCmd(`git push -u origin ${branch}`);
  return `<green>Tasks saved to git repository</green>`;
};

export const pull = async () => {
  await shellCmd("git pull");
  return `<green>Tasks retrieved from git repository</green>`;
};

const shellCmd = async (command: string, cwd = KOI_DIRECTORY) => {
  terminal.log(`<gray>${command}</gray>`);
  const process = Deno.run({
    cwd,
    cmd: command.split(" "),
    stderr: "piped",
    stdout: "piped",
  });
  const stdout = new TextDecoder().decode(await process.output());
  stdout?.split("\n").map(line => terminal.log(`<dim>SHELL: ${line}</dim>`));
  const stderr = new TextDecoder().decode(await process.stderrOutput());
  stderr?.split("\n").map(line => terminal.log(`<dim>SHELL: ${line}</dim>`));
  return await process.status();
};
